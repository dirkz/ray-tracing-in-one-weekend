# Ray Tracing in One Weekend

[F# edition](https://learn.microsoft.com/en-us/dotnet/fsharp/what-is-fsharp), dotnet 8.0.

Implementation of [Ray Tracing in One Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html),
from the [Ray Tracing in One Weekend Series](https://raytracing.github.io/), until (and including)
parts of [Chapter 12, Camera](https://raytracing.github.io/books/RayTracingInOneWeekend.html#positionablecamera/cameraviewinggeometry).

Using [System.Numerics](https://learn.microsoft.com/en-us/dotnet/api/system.numerics?view=net-8.0)
for the vector math.
Which in turn means that everything is based on [float32](https://learn.microsoft.com/en-us/dotnet/api/system.single?view=net-8.0).
 