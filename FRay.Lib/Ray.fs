namespace FRay

open System.Numerics

type Ray =
    struct
        val Origin: Vector3
        val Direction: Vector3

        new(origin, direction) =
            { Origin = origin
              Direction = direction }

        member a.at(t: float32) = a.Origin + t * a.Direction
    end
