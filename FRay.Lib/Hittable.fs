namespace FRay

open System.Numerics

type ScatterInfo =
    struct
        val Attenuation: Vector3
        val Ray: Ray

        new(attenuation, ray) = { Attenuation = attenuation; Ray = ray }
    end

type Material =
    abstract member Scatter: ray: Ray * hr: HitRecord -> ScatterInfo option

and HitRecord =
    struct
        val Point: Vector3
        val Normal: Vector3
        val T: float32
        val FrontFace: bool
        val Material: Material

        new(point, normal, r: Ray, t, material) =
            let frontFace = Vector3.Dot(r.Direction, normal) < 0f
            let normal = if frontFace then normal else -normal

            { Point = point
              Normal = normal
              T = t
              FrontFace = frontFace
              Material = material }
    end

type Hittable =
    abstract member Hit: r: Ray * rayT: Interval -> HitRecord option

type HittableSeq(hittables: seq<Hittable>) =
    interface Hittable with
        member _.Hit(r, rayt) =
            let folder (hitOrNone: HitRecord option) (hittable: Hittable) =
                let tmax =
                    match hitOrNone with
                    | Some hitRecord -> hitRecord.T
                    | None -> rayt.Max

                let rayt = Interval(rayt.Min, tmax)

                match hittable.Hit(r, rayt) with
                | None -> hitOrNone
                | Some hr -> Some hr

            hittables |> Seq.fold folder None
