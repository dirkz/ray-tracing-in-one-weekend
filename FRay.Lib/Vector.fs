namespace FRay

open System
open System.Numerics

[<AutoOpen>]
module VectorFn =
    type Vector3 with

        static member Random() =
            Vector3(randomNumber (), randomNumber (), randomNumber ())

        static member RandomBetween(low, high) =
            Vector3(randomNumberBetween low high, randomNumberBetween low high, randomNumberBetween low high)

        static member RandomInUnitSphere() =
            Vector3.Normalize(Vector3.RandomBetween(-1f, 1f))

        member normal.RandomOnHemisphere() =
            let v = Vector3.RandomInUnitSphere()
            let dot = Vector3.Dot(v, normal)
            if dot > 0f then v else -v

        member v.NearZero() =
            let threshold = Single.Epsilon * 5f
            abs v.X < threshold && abs v.Y < threshold && abs v.Z < threshold

        member v.ReflectWithNormal(n) = v - 2f * Vector3.Dot(v, n) * n

        member uv.Refract(n, etaPrimeOverEta: float32) =
            let cosTheta = min (Vector3.Dot(-uv, n)) 1f
            let rPerp: Vector3 = etaPrimeOverEta * (uv + cosTheta * n)
            let rParall = n * - sqrt(abs <| 1f - rPerp.LengthSquared())
            rPerp + rParall
