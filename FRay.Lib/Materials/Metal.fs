namespace FRay

open System.Numerics

type Metal =
    struct
        val Albedo: Vector3
        val Fuzz: float32

        new(albedo, fuzz) = { Albedo = albedo; Fuzz = fuzz }

        interface Material with
            member this.Scatter(ray, hr) =
                let reflected = ray.Direction.ReflectWithNormal(hr.Normal)

                let reflected =
                    Vector3.Normalize(reflected) + this.Fuzz * Vector3.RandomInUnitSphere()

                if Vector3.Dot(reflected, hr.Normal) > 0f then
                    let ray = Ray(hr.Point, reflected)
                    Some(ScatterInfo(this.Albedo, ray))
                else
                    None
    end
