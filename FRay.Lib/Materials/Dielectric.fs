namespace FRay

open System.Numerics

module DielectricFn =
    let reflectance cosine refractionIndex =
        let r0 = (1f - refractionIndex) / (1f + refractionIndex)
        let r0 = r0 * r0
        r0 + (1f - r0) * pown (1f - cosine) 5

type Dielectric =
    struct
        val RefractionIndex: float32

        new(refractionIndex) = { RefractionIndex = refractionIndex }

        interface Material with
            member this.Scatter(ray, hr) =
                let att = color 1f 1f 1f

                let refractionIndex =
                    if hr.FrontFace then
                        1f / this.RefractionIndex
                    else
                        this.RefractionIndex

                let unitDirection = Vector3.Normalize(ray.Direction)

                let cosTheta = min (Vector3.Dot(-unitDirection, hr.Normal)) 1f
                let sinTheta = sqrt (1f - cosTheta * cosTheta)

                let cannotRefract = refractionIndex * sinTheta > 1f

                let direction =
                    if
                        cannotRefract
                        || DielectricFn.reflectance cosTheta refractionIndex > randomNumber ()
                    then
                        unitDirection.ReflectWithNormal(hr.Normal)
                    else
                        unitDirection.Refract(hr.Normal, refractionIndex)

                let ray = Ray(hr.Point, direction)

                let si = ScatterInfo(att, ray)

                Some si
    end
