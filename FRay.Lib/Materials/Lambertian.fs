namespace FRay

open System.Numerics

type Lambertian =
    struct
        val Albedo: Vector3

        new(albedo) = { Albedo = albedo }

        interface Material with
            member this.Scatter(ray, hr) =
                let direction = hr.Normal + Vector3.RandomInUnitSphere()
                let direction = if direction.NearZero() then hr.Normal else direction
                let ray = Ray(hr.Point, direction)
                Some(ScatterInfo(this.Albedo, ray))
    end
