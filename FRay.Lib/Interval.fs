namespace FRay

open System

type Interval(min, max) =
    member _.Min = min
    member _.Max = max

    new() = Interval(Single.PositiveInfinity, Single.NegativeInfinity)

    member _.Contains(x) = min <= x && x <= max
    member _.Surrounds(x) = min < x && x < max

    member _.Clamp(x) =
        if x < min then min
        else if x > max then max
        else x

    static member Empty = Interval(Single.PositiveInfinity, Single.NegativeInfinity)
    static member Universe = Interval(Single.NegativeInfinity, Single.PositiveInfinity)
    static member FrontUniverse = Interval(0f, Single.PositiveInfinity)
