namespace FRay

open System

[<AutoOpen>]
module UtilFn =
    let degreesToRadians degrees = degrees * float32 Math.PI / 180f
