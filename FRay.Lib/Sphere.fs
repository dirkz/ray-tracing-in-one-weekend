namespace FRay

open System.Numerics

type Sphere =
    struct
        val Center: Vector3
        val Radius: float32
        val Material: Material

        new(center, radius, material) =
            { Center = center
              Radius = radius
              Material = material }

        interface Hittable with
            member this.Hit(r, rayt) =
                let oc = this.Center - r.Origin

                let a = r.Direction.LengthSquared()
                let h = Vector3.Dot(r.Direction, oc)
                let c = oc.LengthSquared() - this.Radius * this.Radius

                let discriminant = h * h - a * c

                if discriminant < 0f then
                    // no hit at all
                    None
                else
                    let sqrtd = sqrt discriminant

                    let mkHitRecord t (r: Ray) center radius material =
                        let position = r.at (t)
                        let normal = (position - center) / radius
                        HitRecord(position, normal, r, t, material)

                    let root1 = (h - sqrtd) / a

                    if not (rayt.Surrounds(root1)) then
                        let root2 = (h + sqrtd) / a

                        if not (rayt.Surrounds(root2)) then
                            None
                        else
                            mkHitRecord root2 r this.Center this.Radius this.Material |> Some
                    else
                        mkHitRecord root1 r this.Center this.Radius this.Material |> Some
    end
