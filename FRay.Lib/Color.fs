﻿namespace FRay

open System.Numerics

[<AutoOpen>]
module ColorFn =
    let color r g b = Vector3(r, g, b)

    let intensity = Interval(0f, 0.999f)

    let linearToGamma linear = if linear > 0f then sqrt linear else 0f

    let writeColor (color: Vector3) =
        let r = color.X |> linearToGamma
        let g = color.Y |> linearToGamma
        let b = color.Z |> linearToGamma

        let rbyte = int <| 256f * intensity.Clamp(r)
        let gbyte = int <| 256f * intensity.Clamp(g)
        let bbyte = int <| 256f * intensity.Clamp(b)

        printfn "%d %d %d" rbyte gbyte bbyte
