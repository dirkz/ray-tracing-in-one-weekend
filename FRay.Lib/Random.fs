namespace FRay

[<AutoOpen>]
module RandomFn =
    let random = lazy (System.Random())

    /// Random number in [0, 1)
    let randomNumber () = random.Value.NextSingle()

    /// Random number in [low, high)
    let randomNumberBetween low high = low + (high - low) * randomNumber ()
