namespace FRay

open System.Numerics

type Camera(aspectRatio, vfov, imageWidth, samplesPerPixel) =
    let imgHeight = float32 imageWidth / aspectRatio |> int
    let imgHeight = if imgHeight < 1 then 1 else imgHeight

    let focalLength = 1f
    let theta = degreesToRadians vfov
    let h = tan <| theta / 2f
    let viewportHeight = 2f * h * focalLength
    let viewportWidth = viewportHeight * (float32 imageWidth / float32 imgHeight)
    let cameraCenter = Vector3.Zero

    let viewportU = Vector3(viewportWidth, 0f, 0f)
    let viewportV = Vector3(0f, -viewportHeight, 0f)

    let pixelDeltaU = viewportU / float32 imageWidth
    let pixelDeltaV = viewportV / float32 imgHeight

    let viewportUpperLeft =
        cameraCenter - Vector3(0f, 0f, focalLength) - viewportU / 2f - viewportV / 2f

    let pixel00Loc = viewportUpperLeft + pixelDeltaU / 2f + pixelDeltaV / 2f

    let pixelSampleScale = 1f / float32 samplesPerPixel

    let maxBounceDepth = 10

    let sampleSquare () =
        Vector3(randomNumber () - 0.5f, randomNumber () - 0.5f, 0f)

    let getRay i j =
        let offset = sampleSquare ()

        let pixelSample =
            pixel00Loc
            + (float32 i + offset.X) * pixelDeltaU
            + (float32 j + offset.Y) * pixelDeltaV

        let rayOrigin = cameraCenter
        let rayDirection = pixelSample - rayOrigin

        Ray(rayOrigin, rayDirection)

    let frontInterval = Interval(0.001f, System.Single.PositiveInfinity)

    let rec rayColor depth (r: Ray) (world: Hittable) =
        match world.Hit(r, frontInterval) with
        | Some hr ->
            if depth = 0 then
                color 0f 0f 0f
            else
                match hr.Material.Scatter(r, hr) with
                | None -> color 0f 0f 0f
                | Some scatterInfo -> scatterInfo.Attenuation * rayColor (depth - 1) scatterInfo.Ray world
        | None ->
            // Sky
            let unitDirection = Vector3.Normalize(r.Direction)
            let a = (unitDirection.Y + 1f) / 2f
            Vector3.Lerp(Vector3(1f, 1f, 1f), Vector3(0.5f, 0.7f, 1f), a)

    member _.Render(world: Hittable) =
        printfn "P3\n%d %d\n255" imageWidth imgHeight

        for j in 0 .. imgHeight - 1 do
            eprintfn "\rScanlines remaining %d" (imgHeight - j)

            for i in 0 .. imageWidth - 1 do
                let mutable pixelColor = color 0f 0f 0f

                for _ in 0..samplesPerPixel do
                    let r = getRay i j
                    pixelColor <- pixelColor + rayColor maxBounceDepth r world

                let pixelCenter = pixel00Loc + (float32 i) * pixelDeltaU + (float32 j) * pixelDeltaV
                let ray = Ray(cameraCenter, -cameraCenter + pixelCenter)
                let sampledColor = pixelSampleScale * pixelColor
                writeColor sampledColor

        eprintfn "\rDone"
