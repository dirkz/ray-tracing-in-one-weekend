﻿open FRay

open System.Numerics

open Argu

type Arguments =
    | Image_Width of int
    | Sample_Size of int

    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Image_Width _ -> "the image width in pixels"
            | Sample_Size _ -> "the number of random samples per ray"

[<EntryPoint>]
let main args =
    let parser = ArgumentParser<Arguments>(programName = "ray-tracing-in-one-weekend")
    let result = parser.Parse args

    let imageWidth = result.GetResult(Image_Width, defaultValue = 400)
    let sampleSize = result.GetResult(Sample_Size, defaultValue = 10)

    let aspectRatio = 16f / 9f

    let r = cos <| float32 System.Math.PI / 4f

    let materialLeft = Lambertian(color 0f 0f 1f)
    let materialRight = Lambertian(color 1f 0f 0f)

    let world =
        [| Sphere(Vector3(-r, 0f, -1f), r, materialLeft)
           Sphere(Vector3(r, 0f, -1f), r, materialRight) |]
        |> Array.map (fun o -> o :> Hittable)

    let world = HittableSeq(world)

    let camera = Camera(aspectRatio, 90f, imageWidth, sampleSize)
    camera.Render(world)

    0
